---
title: "Introduction to R and R Markdown"
author: "John McLevey"
date: "January 11, 2016"
output:
  word_document: default
  pdf_document:
    number_sections: yes
    pandoc_args: --bibliography=/Users/johnmclevey/Documents/bibliography/references.bib
    toc: no
---

```{r setup, include=FALSE}
library(knitr)
library(ggplot2)
library(dplyr)
library(foreign)

# knitr uses png by default, because they are better for html documents
# we want to produce pdf documents, so let's change the default
# this will apply to ALL code chunks in the R Markdown file
opts_chunk$set(dev = 'pdf',
               fig.width = 11,
               fig.height = 11,
               warning=FALSE,
               message=FALSE)

# set your working directory! 
setwd('/Users/johnmclevey/Dropbox/teaching/courses/data_analysis/')
```

> You can write block quotes like this. Although there are other ways to include an abstract, this is quick and easy way that will get the look you want in the final PDF. 

# Markdown

This is an R Markdown file. It's a plain text file that combines the code of your analysis with your text. When you "knit" this file, knitr will replace the code chuncks with the results of the code. There are many ways to customize your source files and their output. For now, we will mostly use the R Studio defaults.

<!--
This is a comment. 
-->

In Markdown, you write headings and subheadings with # signs. You indicate a level 1 header with #, level 2 ##, etc. You can easily *italicize* or **bold** text. It is also very easy to write footnotes.[^example]

[^example]: This is an example of a footnote. 

## Adding Citations to R Markdown Files

It is very simple to use references in R Markdown files [e.g. @bellotti2012getting], although the default pandoc settings in R Studio (i.e. when you click the "knit" button) will not process them correctly. In order to use citations, you will need a bibtex file, and you will need to add some extra lines to the metadata at the top of the article. Your bibtex file will include unique ids for the the books and articles that you cite. You will use that unique ID in the R Markdown file. @freeman1979centrality is a random citation. 

There are lots of different ways you can work with citations. For example, you can reference @Andersen2008 in a sentence without typing out the names of the authors. Or you could write out the full name of the author -- say, Elisa Bellotti [-@bellotti2012getting] -- and only generate the year. You can include multuple citations in a single string at the end of a sentence [e.g. @bellotti2012getting; @Andersen2008; @cote2009untangling; @Diani2015]. 

It is also possible to `knit` your R Markdown file to a regular markdown file and then convert that file into a PDF using pandoc on the command line. If you want to do that, I am happy to show you how. 

There are endless ways to customize things. For example, you can specify templates that will change the look of the final PDF. For now, let's keep it simple and focus on the workflow itself. 

# R Code 

You can add R code to your R Markdown files inside a "code chunk." You can specify how you R to execute those code chunks for the entire documents, or for each individual code chunk. The default settings, which will we use for now, is to print your code and the output to the final PDF. Here are a few examples using [ggplot2](http://ggplot2.org), which is one of the R libraries we will be using all term.

```{r}
# first, read in the ISSP data from the environment survey 
# remember that we set a working directory earlier in this file...
issp <- read.dta('issp_environment/data/ZA5500_v2-0-0.dta')
```

Let's make a very simple graph to get started. 

```{r country}
ggplot(issp, aes(V5)) + 
  geom_bar() + 
  coord_flip() +
  facet_wrap(~ V4, ncol = 4) +
  theme_bw()
```

Here's another: 

```{r pressing_problems}
ggplot(issp, aes(V5)) + 
  geom_bar() + 
  coord_flip() +
  facet_wrap(~ PARTY_LR, ncol = 4) +
  theme_bw()
```

Obviously, there are **lots** of things we can do to make these graphs better. We will get into that very soon! 

# References 